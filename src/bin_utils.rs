use std::f64;

/// Return value of bit N in byte buffer (little endian)
pub fn bitval(bit: u8, buffer: &[u8]) -> u8 {
    return ((buffer[buffer.len() - (1 + (bit >> 3)) as usize]) >> (bit & 0x7)) & 0x1;
}

/// Return int value of bits first..last in frame. Exemple: 1..13 for a 12 bits long int
pub fn intval(first: u8, last: u8, buffer: &[u8]) -> u32 {
    let mut sum: u32 = 0;
    for i in first..(last + 1) {
        sum += (bitval(i, &buffer) as u32) << (i - first)
    }
    return sum;
}

/// Round an f64 value to a certain number of digits.
///
/// * `val` - Value to be rounded.
/// * `n` - Number of digits.
///
/// ```
/// use marss::bin_utils::roundn;
///
/// let x = roundn(1.234567, 3);
/// assert_eq!(x, 1.235)
/// ```
pub fn roundn(val: f64, n: u32) -> f64 {
    let r = 10.0_f64.powi(n as i32);
    return (val * r).round() / r;
}

/// Optimized version of `marss::bin_utils::roundn` for 1 digit rounding.
pub fn round1(val: f64) -> f64 {
    return (val * 10.0).round() / 10.0;
}

/// Optimized version of `marss::bin_utils::roundn` for 2 digit rounding.
pub fn round2(val: f64) -> f64 {
    return (val * 100.0).round() / 100.0;
}

/// Optimized version of `marss::bin_utils::roundn` for 3 digit rounding.
pub fn round3(val: f64) -> f64 {
    return (val * 1000.0).round() / 1000.0;
}

#[cfg(test)]
mod tests {
    use bin_utils::{bitval, intval, round1, round2, round3, roundn};

    #[test]
    fn test_bitval() {
        let data1: [u8; 2] = [0xF1, 0x11];

        assert_eq!(1, bitval(0, &data1));
        assert_eq!(0, bitval(1, &data1));
        assert_eq!(0, bitval(2, &data1));
        assert_eq!(0, bitval(3, &data1));
        assert_eq!(1, bitval(4, &data1));
        assert_eq!(0, bitval(5, &data1));
        assert_eq!(0, bitval(6, &data1));
        assert_eq!(0, bitval(7, &data1));
        assert_eq!(1, bitval(8, &data1));
        assert_eq!(0, bitval(9, &data1));
        assert_eq!(0, bitval(10, &data1));
        assert_eq!(0, bitval(11, &data1));
        assert_eq!(1, bitval(12, &data1));
        assert_eq!(1, bitval(13, &data1));
        assert_eq!(1, bitval(14, &data1));
        assert_eq!(1, bitval(15, &data1))
    }

    #[test]
    fn test_intval() {
        let data1: [u8; 2] = [0xF1, 0x11];

        assert_eq!(1, intval(0, 3, &data1));
        assert_eq!(1, intval(4, 7, &data1));
        assert_eq!(0x11, intval(0, 7, &data1));
        assert_eq!(0x111, intval(0, 8, &data1));
        assert_eq!(0x111, intval(0, 11, &data1));
        assert_eq!(0xF1, intval(8, 15, &data1));
        assert_eq!(0xF, intval(12, 15, &data1));
        assert_eq!(0xE, intval(11, 14, &data1));
    }

    #[test]
    #[should_panic]
    fn test_bitval_panic() {
        let data1: [u8; 2] = [0xF1, 0x11];

        assert_eq!(1, bitval(40, &data1))
    }

    #[test]
    fn test_round() {
        assert_eq!(1.1, round1(1.1111111));
        assert_eq!(1.11, round2(1.1111111));
        assert_eq!(1.111, round3(1.1111111));
        assert_eq!(1.111, roundn(1.1111112, 3));
    }
}
