//! # MARSS Rust utilities
//! Contains general utility functions to be used in different MARSS projects.
//! Include marss in your Cargo.toml, either locally or referencing to bitbucket:

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate getopts;

/// Log utils
pub mod log_utils;

/// Parse options
pub mod args;

/// Time related utils
pub mod time_utils;

/// Binary operation utils
pub mod bin_utils;
