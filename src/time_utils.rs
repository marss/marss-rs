
use std::time;

/// Get an EPOCH timestamp in milliseconds.
///
/// ```
/// use marss::time_utils::timestamp_millis;
/// let ts = timestamp_millis();
/// assert!(ts > 0);
/// ```
pub fn timestamp_millis() -> u64 {
    let now = time::SystemTime::now();

    return match now.duration_since(time::UNIX_EPOCH) {
        Ok(d) => d.as_secs() * 1000 + (d.subsec_nanos() / 1000000) as u64,
        Err(e) => panic!("Unable to get timestamp: {}", e),
    };
}

#[cfg(test)]
mod tests {
    use time_utils::timestamp_millis;
    use std::time::Duration;
    use std::thread::sleep;

    #[test]
    fn test_timestamp_millis() {
        for millis in 1..20 {
            let a_millis = timestamp_millis();

            // we sleep for 2 seconds
            sleep(Duration::from_millis(millis * 10));
            let b_millis = timestamp_millis();
            //            println!("{} - {} -> {} == {}", a_millis, millis * 10, b_millis, b_millis - a_millis);
            assert!((b_millis - a_millis) < (millis * 10 + 10));
        }
    }
}
