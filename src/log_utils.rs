
use env_logger;

pub static DEFAULT_LOG_LEVEL: u32 = 20;

/// Initialize application log level.
///
/// Log level is represented as in python, by an integer:
///
/// - 0 = trace
/// - 10 = debug
/// - 20 = info
/// - 30 = warn
/// - 40 = error
///
/// Package name represents the current package, to which to apply the log level:
///
/// ```
/// use marss::log_utils::init_logging;
/// init_logging(20, Option::None);
/// ```
pub fn init_logging(level: u32, debug: Option<String>) {
    use std::env;

    let lvl_string = match level {
        0 => "trace",
        10 => "debug",
        20 => "info",
        30 => "warn",
        40 => "error",
        _ => "error",
    };

    let rust_log = match debug {
        Some(s) => format!("{},{}", lvl_string, s),
        _ => format!("{}", lvl_string)
    };

    info!("Log level set to {:?}", rust_log);
    env::set_var("RUST_LOG", rust_log);

    env_logger::init();
}
