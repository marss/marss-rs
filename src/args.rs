
use log_utils::{init_logging, DEFAULT_LOG_LEVEL};
use std::env;
use getopts::{Options, Matches};
use std::process::exit;

/// Prepare default options for use in parse_args.
pub fn default_options() -> Options {
    let mut opts = Options::new();
    opts.optopt(
        "l",
        "logging",
        "log level (python style, default: 20 == info)",
        "<level>",
    );
    opts.optopt(
        "d",
        "debug",
        "rust debug string (in format path::to::module=<level>)",
        "<debug>",
    );
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("v", "version", "print version");
    return opts;
}

/// Parse command line options.
pub fn parse_args(opts: &Options, version: &str) -> Matches {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let options = match opts.parse(&args[0..]) {
        Ok(m) => m,
        Err(f) => {
            println!("{}", f.to_string());
            exit(1);
        }
    };

    if options.opt_present("h") {
        let brief = format!("Usage: {} [options]", program);
        println!("{}", opts.usage(&brief));
        exit(0);
    }

    if options.opt_present("v") {
        println!("{} {}", program, version);
        exit(0);
    }

    init_logging(
        match options.opt_str("l") {
            Some(lvl) => {
                u32::from_str_radix(lvl.trim(), 10).expect("invalid log level, number expected")
            }
            None => DEFAULT_LOG_LEVEL,
        },
        options.opt_str("d")
    );

    return options;
}
